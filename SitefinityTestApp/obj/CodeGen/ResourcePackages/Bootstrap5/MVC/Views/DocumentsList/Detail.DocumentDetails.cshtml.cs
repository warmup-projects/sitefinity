#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SitefinityTestApp.ResourcePackages.Bootstrap5.MVC.Views.DocumentsList
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    #line 3 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
    using Telerik.Sitefinity.Frontend.Mvc.Helpers;
    
    #line default
    #line hidden
    
    #line 4 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
    using Telerik.Sitefinity.Web.DataResolving;
    
    #line default
    #line hidden
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/ResourcePackages/Bootstrap5/MVC/Views/DocumentsList/Detail.DocumentDetails.csht" +
        "ml")]
    public partial class Detail_DocumentDetails : System.Web.Mvc.WebViewPage<Telerik.Sitefinity.Frontend.Media.Mvc.Models.DocumentsList.DocumentDetailsViewModel>
    {

#line 47 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
public System.Web.WebPages.HelperResult GetFileExtensionCssClass(String extension)
{
#line default
#line hidden
return new System.Web.WebPages.HelperResult(__razor_helper_writer => {

#line 48 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
 

    if (extension == "xlsx")
    {
        

#line default
#line hidden

#line 52 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
WriteTo(__razor_helper_writer, Html.HtmlSanitize("bg-green"));


#line default
#line hidden

#line 52 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                      ;
    }
    else if (extension == "doc" || extension == "docx")
    {
        

#line default
#line hidden

#line 56 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
WriteTo(__razor_helper_writer, Html.HtmlSanitize("bg-blue"));


#line default
#line hidden

#line 56 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                     ;
    }
	else if (extension == "ppt" || extension == "pptx")
    {
        

#line default
#line hidden

#line 60 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
WriteTo(__razor_helper_writer, Html.HtmlSanitize("bg-orange"));


#line default
#line hidden

#line 60 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                       ;
    }
	else if (extension == "pdf")
    {
        

#line default
#line hidden

#line 64 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
WriteTo(__razor_helper_writer, Html.HtmlSanitize("bg-red"));


#line default
#line hidden

#line 64 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                    ;
    }
	else if (extension == "zip")
    {
        

#line default
#line hidden

#line 68 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
WriteTo(__razor_helper_writer, Html.HtmlSanitize("bg-purple"));


#line default
#line hidden

#line 68 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                       ;
    }
	else
	{
        

#line default
#line hidden

#line 72 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
WriteTo(__razor_helper_writer, Html.HtmlSanitize("bg-gray"));


#line default
#line hidden

#line 72 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                     ;
	}


#line default
#line hidden
});

#line 74 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
}
#line default
#line hidden

        public Detail_DocumentDetails()
        {
        }
        public override void Execute()
        {
WriteLiteral("\r\n<div");

WriteAttribute("class", Tuple.Create(" class=\"", 195), Tuple.Create("\"", 218)
            
            #line 6 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
, Tuple.Create(Tuple.Create("", 203), Tuple.Create<System.Object, System.Int32>(Model.CssClass
            
            #line default
            #line hidden
, 203), false)
);

WriteLiteral(" role=\"group\"");

WriteLiteral(">\r\n\r\n    <h1>\r\n");

WriteLiteral("        ");

            
            #line 9 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
   Write(Model.Item.Fields.Title);

            
            #line default
            #line hidden
WriteLiteral("\r\n    </h1>\r\n\r\n    <div");

WriteLiteral(" class=\"text-muted\"");

WriteLiteral(">\r\n        <span");

WriteLiteral(" class=\"visually-hidden\"");

WriteLiteral(">");

            
            #line 13 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                 Write(Html.Resource("PublishedOn"));

            
            #line default
            #line hidden
WriteLiteral(" </span>\r\n");

WriteLiteral("        ");

            
            #line 14 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
   Write(Model.Item.GetDateTime("PublicationDate", "MMM d, yyyy, HH:mm"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("        ");

            
            #line 15 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
   Write(Html.Resource("By"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("        ");

            
            #line 16 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
   Write(DataResolver.Resolve(@Model.Item.DataItem, "Author", null));

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n\r\n");

            
            #line 19 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
    
            
            #line default
            #line hidden
            
            #line 19 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
     if (!string.IsNullOrEmpty(Model.Item.Fields.Description))
    {

            
            #line default
            #line hidden
WriteLiteral("    <div");

WriteLiteral(" class=\"mt-3\"");

WriteAttribute("aria-label", Tuple.Create(" aria-label=\"", 680), Tuple.Create("\"", 727)
            
            #line 21 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
, Tuple.Create(Tuple.Create("", 693), Tuple.Create<System.Object, System.Int32>(Html.Resource(" DocumentSummary")
            
            #line default
            #line hidden
, 693), false)
);

WriteLiteral(">\r\n");

WriteLiteral("        ");

            
            #line 22 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
   Write(Model.Item.Fields.Description);

            
            #line default
            #line hidden
WriteLiteral("\r\n    </div>\r\n");

            
            #line 24 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
    }

            
            #line default
            #line hidden
WriteLiteral("\r\n    <div");

WriteLiteral(" class=\"d-flex gap-3 align-items-center mt-3\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"flex-shrink-0 pt-1\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"fa-layers fa-3x\"");

WriteLiteral(">\r\n                <svg");

WriteLiteral(" class=\"svg-inline--fa fa-w-16\"");

WriteLiteral(">\r\n                    <use");

WriteAttribute("xlink:href", Tuple.Create(" xlink:href=\"", 1030), Tuple.Create("\"", 1107)
, Tuple.Create(Tuple.Create("", 1043), Tuple.Create<System.Object, System.Int32>(Href("~/ResourcePackages/Bootstrap5/assets/dist/sprites/solid.svg#file")
, 1043), false)
);

WriteLiteral(" class=\"fa-secondary\"");

WriteLiteral("></use>\r\n                </svg>\r\n                <span");

WriteAttribute("class", Tuple.Create(" class=\"", 1183), Tuple.Create("\"", 1341)
, Tuple.Create(Tuple.Create("", 1191), Tuple.Create("fa-layers-text", 1191), true)
, Tuple.Create(Tuple.Create(" ", 1205), Tuple.Create("fa-layers-bottom-right", 1206), true)
, Tuple.Create(Tuple.Create(" ", 1228), Tuple.Create("text-uppercase", 1229), true)
, Tuple.Create(Tuple.Create(" ", 1243), Tuple.Create("ps-3", 1244), true)
, Tuple.Create(Tuple.Create(" ", 1248), Tuple.Create("pe-3", 1249), true)
, Tuple.Create(Tuple.Create(" ", 1253), Tuple.Create("mb-2", 1254), true)
, Tuple.Create(Tuple.Create(" ", 1258), Tuple.Create("text-white", 1259), true)
            
            #line 32 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                           , Tuple.Create(Tuple.Create(" ", 1269), Tuple.Create<System.Object, System.Int32>(GetFileExtensionCssClass(Model.Extension)
            
            #line default
            #line hidden
, 1270), false)
, Tuple.Create(Tuple.Create(" ", 1312), Tuple.Create("sf-icon-txt-", 1313), true)
            
            #line 32 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                                                                   , Tuple.Create(Tuple.Create("", 1325), Tuple.Create<System.Object, System.Int32>(Model.Extension
            
            #line default
            #line hidden
, 1325), false)
);

WriteLiteral(">");

            
            #line 32 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                                                                                                                                                                Write(Model.Extension);

            
            #line default
            #line hidden
WriteLiteral("</span>\r\n            </div>\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"flex-grow-1\"");

WriteLiteral(" role=\"group\"");

WriteLiteral(">\r\n            <div>\r\n                <a");

WriteAttribute("href", Tuple.Create(" href=\"", 1489), Tuple.Create("\"", 1523)
            
            #line 37 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
, Tuple.Create(Tuple.Create("", 1496), Tuple.Create<System.Object, System.Int32>(Model.Item.Fields.MediaUrl
            
            #line default
            #line hidden
, 1496), false)
);

WriteLiteral(" target=\"_blank\"");

WriteLiteral(">");

            
            #line 37 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                                                 Write(Html.Resource("Download"));

            
            #line default
            #line hidden
WriteLiteral("<span");

WriteLiteral(" class=\"visually-hidden\"");

WriteLiteral(">");

            
            #line 37 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                                                                                                         Write(Model.Item.Fields.Title);

            
            #line default
            #line hidden
WriteLiteral("</span></a>\r\n                <span");

WriteLiteral(" class=\"text-muted small\"");

WriteAttribute("aria-label", Tuple.Create(" aria-label=\"", 1680), Tuple.Create("\"", 1725)
            
            #line 38 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
, Tuple.Create(Tuple.Create("", 1693), Tuple.Create<System.Object, System.Int32>(Html.Resource(" FileExtension")
            
            #line default
            #line hidden
, 1693), false)
);

WriteLiteral(">(");

            
            #line 38 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                                                                         Write(Model.Extension);

            
            #line default
            #line hidden
WriteLiteral(")</span>\r\n            </div>\r\n            <span");

WriteLiteral(" class=\"text-muted small\"");

WriteAttribute("aria-label", Tuple.Create(" aria-label=\"", 1816), Tuple.Create("\"", 1856)
            
            #line 40 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
, Tuple.Create(Tuple.Create("", 1829), Tuple.Create<System.Object, System.Int32>(Html.Resource(" FileSize")
            
            #line default
            #line hidden
, 1829), false)
);

WriteLiteral(">");

            
            #line 40 "..\..\ResourcePackages\Bootstrap5\MVC\Views\DocumentsList\Detail.DocumentDetails.cshtml"
                                                                                Write(Math.Ceiling((double)Model.Item.Fields.TotalSize / 1024) + " KB");

            
            #line default
            #line hidden
WriteLiteral("</span>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n");

WriteLiteral("\r\n");

        }
    }
}
#pragma warning restore 1591
